package com.example.google_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var diceImage : ImageView
    lateinit var diceImage2 : ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        diceImage = findViewById(R.id.dice_image)
        diceImage2 = findViewById(R.id.dice_image2)

        val rollButton: Button = findViewById(R.id.rollDieButton)
        rollButton.setOnClickListener{ rollDice() }

        val resetButton: Button = findViewById(R.id.resetButton)
        resetButton.setOnClickListener{ resetVal() }
    }
     private fun getRandomInteger(): Int {

         val imgDice = when ((1..6).random()){
             1 -> R.drawable.dice_1
             2 -> R.drawable.dice_2
             3 -> R.drawable.dice_3
             4 -> R.drawable.dice_4
             5 -> R.drawable.dice_5
             else -> R.drawable.dice_6
         }
        return  imgDice
    }

    private fun rollDice(){
        val dice1Count = getRandomInteger()
        val dice2Count = getRandomInteger()

        diceImage.setImageResource(dice1Count)
        diceImage2.setImageResource(dice2Count)
    }

    private fun resetVal(){
        diceImage.setImageResource(R.drawable.empty_dice)
        diceImage2.setImageResource(R.drawable.empty_dice)
    }
}